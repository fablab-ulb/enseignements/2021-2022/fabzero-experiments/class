# Fog net project at Frugal Lab/FabLab ULB in the Challenge Learning Framework

### Engage - Find a challenge that motivates you

* ***Big ideas - What is the broad theme or concept I would like to explore ?***
    * Unconventional water harvesting

* ***Essential questions - What are the essential questions that reflect personal interests and the needs of the community ?***
    * Can we learn from Nature to create sustainable technologies ?
    * Can we develop low-tech solutions to solve societal problems ?
    * How can we tap into the diversity of people and of disciplines empowered by technologies (communication, fabrication, and management) to help achieve [the UN 17 Sustainable Development Goals](https://sdgs.un.org/goals) ?

* ***Challenge - what is your call for action ?***
    * How to produce efficient, afordable and durable fog nets using frugality and the fablab environment as creative constraints ?

### Investigate - step on the shoulders of the giants not on their toes

* ***Guiding questions - what are all the questions that needs to be answered in order to tackle the challenge ? Priority ?***
    * In what region and for which community this technology can be useful ? What are their needs ?
    * What are the existing human technologies that allows to harvest atmospheric moisture ? How do they work ?
    * What are the strategies that nature uses to harvest water ?
    * What is the role of atmospheric moisture in ecosystems ?
    * How frugal/low-tech, open-source and collaborative projects can be deployed ? How can we value them ?

* ***Guiding activities/ressources - What resources can I use to answer those questions ? Science.***
    * [Our expertise](https://frugal.ulb.be/) on fluid-structure interaction and bioinspiration.
    * Our interdiscplinary team of experts at FabLab ULB and our international network of scientists and makers.
    * [AskNature](https://asknature.org/?s=collect%20water&page=0&is_v=1)
    * scientific state of the art on water harvesting strategies [[Beysens](https://link.springer.com/book/10.1007/978-3-030-90442-5?noAccess=true),...]
    * open-source software and hardware community ([Open-Source Lab](https://www.elsevier.com/books/open-source-lab/pearce/978-0-12-410462-4),...)
    * open science movement
    * frugal science project development [[Stanford class](https://www.frugalscience.org/), [Foldscope](https://www.foldscope.com/), Jaipurfoot ([website](https://jaipurfoot.org/), [paper](https://link.springer.com/article/10.1007/s10439-013-0792-8)),...]
    * [Dar Si Hmad NGO](http://darsihmad.org/fr/) (Morocco)

* ***Analysis and synthesis - write a summary of your findings, facts and data collected***

  * How many people are concerned about water scarcity ?
  * where is fresh water/fog on earth ?
  * where are fog nets located ?
  * How much water a person needs?
  * How much water can be collected by a fog net ?
  * How much cost a fog net ? How much cost water collected by fog nets ?
  * What makes an efficient fog net ?

### Act - Develop a solution, implement it and get feedback

* ***Solution concepts - What is your solution about ?***

    * Making a solution from a simple sheet of material (PET,...) with folds and cuts and accessible tools (fablab) -> kirigami fog nets.

* ***Solution development - how do this solution solve the challenge ? Prototype and develop.***

    * We started by developing an experimental test bench to measure and compare fog net efficiencies.
    * We rapid prototyped and tested some kirigami fog nets that revealed to be as efficient as the best fog nets on the market but potentially 10 times cheaper.
    * We understand the mechanism of how it works so we understand the fundamental rules of design.
    * We search for places to make field tests. We found a NGO that deploy the largest fog nets field in the world.
    * We developed 1 meter square test samples to be deployed on field.
    * We search for an affordable way of producing kirigami fog nets.
    * We search for an open-source economical model to sustain the deployement of the fog nets in communities that need it.

* ***Implementation and evaluation - Experiment and evaluate the solution.***

    * Experimental test bench
    * Field measurement
    * Fabrication cost
    * Ecological impact
    * Community empowerment
    * Sustainable business model
